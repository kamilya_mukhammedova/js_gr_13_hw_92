const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');
const {nanoid} = require("nanoid");
const Message = require("./models/Message");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [user1, user2] = await User.create({
    email: 'jjj55@mail.ru',
    password: '123',
    token: nanoid(),
    displayName: 'Jhon Doe'
  }, {
    email: 'admin@gmail.com',
    password: '123',
    token: nanoid(),
    displayName: 'Admin'
  });

  await Message.create({
    text: 'Hello there',
    user: user1
  },{
    text: 'How are you, guys?',
    user: user1
  }, {
    text: 'Hi! Great!',
    user: user2
  }, {
    text: 'Good day!',
    user: user2
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));
