import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user.model';

interface Message {
  text: string,
  user: {
    id: string,
    displayName: string
  }
}

interface ServerMessage {
  type: string,
  message?: Message,
  prevMessages?: Message[],
  displayName?: string
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnInit, OnDestroy {
  ws!: WebSocket;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  token: string = '';
  userId: string = '';
  userDisplayName: string = '';
  displayNameArray: string[] = [];
  messageText = '';
  messagesArray: Message[] = [];

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.ws = new WebSocket('ws://localhost:8060/chat');
    this.userSubscription = this.user.subscribe(user => {
      if (user) {
        this.token = user.token;
        this.userId = user._id;
        this.userDisplayName = user.displayName;
      }
      // setTimeout(() => {
      //   this.ws.close()
      // }, 2000)
    });

    this.ws.onclose = () => {
      console.log('Ws closed');
      this.ws = new WebSocket('ws://localhost:8060/chat');
      if (this.token) {
        setTimeout(() => {
          console.log('Ws connected one more time');
            this.ws.send(JSON.stringify({
              type: 'LOGIN',
              token: this.token
            }));
        }, 2000);
      }
    };

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);
      if (decodedMessage.type === 'PREVIOUS_MESSAGES') {
        if (decodedMessage.prevMessages) {
          this.messagesArray = decodedMessage.prevMessages;
        }
      }

      if (decodedMessage.type === 'GET_DISPLAY_NAME') {
        if (decodedMessage.displayName != null) {
          this.displayNameArray.push(decodedMessage.displayName);
        }
      }

      if (decodedMessage.type === 'NEW_MESSAGE') {
        if (decodedMessage.message) {
          this.messagesArray.push(decodedMessage.message);
        }
      }
    }

    this.ws.onopen = () => {
      console.log('open')
      this.ws.send(JSON.stringify({
        type: 'LOGIN',
        token: this.token
      }));
    }
  }


  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      text: this.messageText,
      user: this.userId
    }));
    this.messageText = '';
  }

  ngOnDestroy(): void {
    this.ws.close();
    this.userSubscription.unsubscribe();
    this.token = '';
  }
}
