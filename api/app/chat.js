const {nanoid} = require("nanoid");
const User = require("../models/User");
const Message = require("../models/Message");
const activeConnections = {};

module.exports = async (ws, req) => {
  const id = nanoid();
  console.log('Client connected! id = ', id);
  activeConnections[id] = ws;

  let user = null;
  const prevMessagesArray = [];

  const prevMessages = await Message.find().populate('user', '_id displayName').limit(30);
  for (let i = 0; i < prevMessages.length; i++) {
    const obj = {
      text: prevMessages[i].text,
      user: {id: prevMessages[i].user._id, displayName: prevMessages[i].user.displayName}
    };
    prevMessagesArray.push(obj);
  }

  ws.send(JSON.stringify({
    type: 'PREVIOUS_MESSAGES',
    prevMessages: prevMessagesArray
  }));

  ws.on('message', async msg => {
    const decodedMessage = JSON.parse(msg);
    switch (decodedMessage.type) {
      case 'LOGIN':
        const currentUser = await User.findOne({token: decodedMessage.token});
        if (!currentUser) break;
        user = currentUser;

        ws.send(JSON.stringify({
          type: 'GET_DISPLAY_NAME',
          displayName: user.displayName
        }));
        break;
      case 'SEND_MESSAGE':
        if (user === null) break;
        const newMsg = new Message({
          text: decodedMessage.text,
          user: decodedMessage.user,
        });
        await newMsg.save();
        const messageToChat = await Message.findOne({_id: newMsg._id}).populate('user', '_id displayName');
        Object.keys(activeConnections).forEach(id => {
          const conn = activeConnections[id];

          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: messageToChat
          }));
        });
        break;
      default:
        console.log('Unknown type: ', decodedMessage.type);
    }
  });

  ws.on('close', () => {
    console.log('Client disconnected! id = ', id);
    delete activeConnections[id];
  });
};